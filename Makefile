RELEASE=6.0

GRUB_DIR=grub
GRUB_SRC=${GRUB_DIR}.tar.gz
TAG=debian/2.02+dfsg1-18

# update/sync changelog.pve
DEBVER=2.02+dfsg1-18-pve1

PC_DEBS=					\
grub2_${DEBVER}_amd64.deb			\
grub2-common_${DEBVER}_amd64.deb		\
grub-common_${DEBVER}_amd64.deb			\
grub-efi-amd64-bin_${DEBVER}_amd64.deb		\
grub-pc_${DEBVER}_amd64.deb			\
grub-pc-bin_${DEBVER}_amd64.deb			\
grub-pc-dbg_${DEBVER}_amd64.deb			\
grub-rescue-pc_${DEBVER}_amd64.deb		\
grub-theme-starfield_${DEBVER}_amd64.deb

EFI_DEBS=					\
grub-efi_${DEBVER}_amd64.deb			\
grub-efi-amd64_${DEBVER}_amd64.deb		\
grub-efi-ia32_${DEBVER}_amd64.deb		\
grub-efi-ia32-bin_${DEBVER}_amd64.deb		\

DEBS=${PC_DEBS} ${EFI_DEBS}

all: ${DEBS}

.PHONY: dinstall
dinstall: ${DEBS}
	dpkg -i ${PC_DEBS}

${DEBS}: deb
.PHONY: deb
deb:
	rm -rf ${GRUB_DIR}
	tar xf ${GRUB_SRC}
	mv ${GRUB_DIR}/debian/changelog ${GRUB_DIR}/debian/changelog.org
	cat changelog.pve ${GRUB_DIR}/debian/changelog.org > ${GRUB_DIR}/debian/changelog
	cd ${GRUB_DIR}; ln -s ../pvepatches patches
	cd ${GRUB_DIR}; quilt push -a
	# hack: remove quilt dir, so that dpkg-buildpackage correctly apply
	# all debian patches
	rm -rf ${GRUB_DIR}/.pc ${GRUB_DIR}/patches
	cd ${GRUB_DIR}; dpkg-buildpackage -b -uc -us

.PHONY: download
download:
	rm -rf grub ${GRUB_SRC} ${GRUB_SRC}.tmp
	git clone --recursive -b ${TAG} https://salsa.debian.org/grub-team/grub.git
	tar czf ${GRUB_SRC}.tmp grub
	mv ${GRUB_SRC}.tmp ${GRUB_SRC}

.PHONY: upload
upload: ${DEBS}
	tar cf - ${DEBS} | ssh repoman@repo.proxmox.com -- upload --product pve --dist stretch --arch amd64

.PHONY: distclean
distclean: clean

clean:
	rm -rf *~ *.deb *.udeb *.changes *.buildinfo ${GRUB_DIR}
